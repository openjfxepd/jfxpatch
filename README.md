# OpenJFX Patches

This project is a Java class library containing patches to the OpenJFX common code.

## Project

The NetBeans project uses a Java platform called “JDK 1.8 for x86egl” that contains the Oracle JDK 8 installation with the OpenJFX x86egl bundle overlay.

## Installation

Place the class library on the target device in `$HOME/lib/ext/jfxpatch.jar`.

Add the Java system property `-Djava.ext.dirs=$HOME/lib/ext:$JAVA_HOME/jre/lib/ext` to pick up the patched classes, where `JAVA_HOME` is set to the location of your JDK 8 installation with the OpenJFX overlay bundle.

