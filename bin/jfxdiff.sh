#!/bin/bash
# Prints the OpenJFX patches under the "src" directory
trap exit INT TERM

# Directory of the local OpenJFX 8 repository
: "${OPENJFX:?}"

files="\
    com/sun/glass/ui/Pixels.java \
    com/sun/glass/ui/monocle/LinuxEventBuffer.java \
    com/sun/glass/ui/monocle/LinuxInputDevice.java \
    com/sun/prism/impl/QueuedPixelSource.java" 

for f in $files; do
    printf "\n==> $f\n\n"
    diff $OPENJFX/modules/graphics/src/main/java/$f src/$f
done
